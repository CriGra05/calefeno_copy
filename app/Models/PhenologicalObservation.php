<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

//belongsTo is a type of relationship in Laravel's Eloquent ORM. It defines a one-to-many relationship between two database tables, where one record in the current table belongs to another record in a related table.
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class PhenologicalObservation extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'phenological_observations';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'plant_observed_id',
        'notes',
        'weather_conditions',
        'blossoming_time_id',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function plant_observed()
    {
        return $this->belongsTo(PlantObserved::class, 'plant_observed_id');
    }

    public function blossoming_time()
    {
        return $this->belongsTo(BlossomingTime::class, 'blossoming_time_id');
    }

    public function user()
    {
    return $this->belongsTo(User::class);
    }




}
