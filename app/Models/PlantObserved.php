<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlantObserved extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'plant_observeds';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'plant_observed_name',
        'notes',
        'altitude',
        'coordinates',
        'sun_exposure_id',
        'slope_id',
        'sorroundings_id',
        'plant_name_id',
        'observed_zone_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function sun_exposure()
    {
        return $this->belongsTo(SunExposure::class, 'sun_exposure_id');
    }

    public function slope()
    {
        return $this->belongsTo(Slope::class, 'slope_id');
    }

    public function sorroundings()
    {
        return $this->belongsTo(Sorrounding::class, 'sorroundings_id');
    }

    public function plant_name()
    {
        return $this->belongsTo(Plant::class, 'plant_name_id');
    }

    public function observed_zone()
    {
        return $this->belongsTo(ObservationZone::class, 'observed_zone_id');
    }
    // public function phenological_observations()
    // {
    //     return $this->hasMany(PhenologicalObservation::class, 'plant_observed_id');
    // }

}
