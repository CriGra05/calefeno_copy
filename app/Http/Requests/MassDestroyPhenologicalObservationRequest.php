<?php

namespace App\Http\Requests;

use App\Models\PhenologicalObservation;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyPhenologicalObservationRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('phenological_observation_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:phenological_observations,id',
        ];
    }
}
