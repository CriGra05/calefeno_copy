<?php

namespace App\Http\Requests;

use App\Models\BlossomingTime;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateBlossomingTimeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('blossoming_time_edit');
    }

    public function rules()
    {
        return [
            'name_blossoming_time' => [
                'string',
                'required',
                'unique:blossoming_times,name_blossoming_time,' . request()->route('blossoming_time')->id,
            ],
            'notes' => [
                'string',
                'nullable',
            ],
        ];
    }
}
