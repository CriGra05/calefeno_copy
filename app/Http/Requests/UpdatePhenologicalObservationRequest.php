<?php

namespace App\Http\Requests;

use App\Models\PhenologicalObservation;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdatePhenologicalObservationRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('phenological_observation_edit');
    }

    public function rules()
    {
        return [
            'notes' => [
                'string',
                'nullable',
            ],
            'weather_conditions' => [
                'string',
                'nullable',
            ],
        ];
    }
}
