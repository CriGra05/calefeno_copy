<?php

namespace App\Http\Requests;

use App\Models\BloomStartPeriod;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreBloomStartPeriodRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('bloom_start_period_create');
    }

    public function rules()
    {
        return [
            'bloom_start_period' => [
                'string',
                'nullable',
            ],
            'observation' => [
                'string',
                'nullable',
            ],
        ];
    }
}
