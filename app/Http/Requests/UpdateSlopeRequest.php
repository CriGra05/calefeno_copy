<?php

namespace App\Http\Requests;

use App\Models\Slope;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateSlopeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('slope_edit');
    }

    public function rules()
    {
        return [
            'terrain_slope' => [
                'string',
                'nullable',
            ],
        ];
    }
}
