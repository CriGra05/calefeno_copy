<?php

namespace App\Http\Requests;

use App\Models\ObservationZone;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateObservationZoneRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('observation_zone_edit');
    }

    public function rules()
    {
        return [
            'zone_name' => [
                'string',
                'nullable',
            ],
            'zip_code' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
