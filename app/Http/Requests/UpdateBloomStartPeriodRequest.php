<?php

namespace App\Http\Requests;

use App\Models\BloomStartPeriod;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateBloomStartPeriodRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('bloom_start_period_edit');
    }

    public function rules()
    {
        return [
            'bloom_start_period' => [
                'string',
                'nullable',
            ],
            'observation' => [
                'string',
                'nullable',
            ],
        ];
    }
}
