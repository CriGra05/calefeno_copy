<?php

namespace App\Http\Requests;

use App\Models\Sorrounding;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroySorroundingRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('sorrounding_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:sorroundings,id',
        ];
    }
}
