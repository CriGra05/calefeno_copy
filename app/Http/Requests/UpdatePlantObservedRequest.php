<?php

namespace App\Http\Requests;

use App\Models\PlantObserved;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdatePlantObservedRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('plant_observed_edit');
    }

    public function rules()
    {
        return [
            'notes' => [
                'string',
                'nullable',
            ],
            'altitude' => [
                'string',
                'nullable',
            ],
            'coordinates' => [
                'string',
                'nullable',
            ],
            'plant_observed_name' => [
                'required',
                'nullable'
            ]
        ];
    }
}
