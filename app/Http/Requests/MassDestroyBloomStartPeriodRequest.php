<?php

namespace App\Http\Requests;

use App\Models\BloomStartPeriod;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyBloomStartPeriodRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('bloom_start_period_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:bloom_start_periods,id',
        ];
    }
}
