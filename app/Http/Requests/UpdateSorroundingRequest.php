<?php

namespace App\Http\Requests;

use App\Models\Sorrounding;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateSorroundingRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('sorrounding_edit');
    }

    public function rules()
    {
        return [
            'sorroundings' => [
                'string',
                'nullable',
            ],
            'description' => [
                'string',
                'nullable',
            ],
        ];
    }
}
