<?php

namespace App\Http\Requests;

use App\Models\PlantObserved;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyPlantObservedRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('plant_observed_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:plant_observeds,id',
        ];
    }
}
