<?php

namespace App\Http\Requests;

use App\Models\SunExposure;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreSunExposureRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('sun_exposure_create');
    }

    public function rules()
    {
        return [
            'sun_exposure' => [
                'string',
                'nullable',
            ],
            'notes' => [
                'string',
                'nullable',
            ],
        ];
    }
}
