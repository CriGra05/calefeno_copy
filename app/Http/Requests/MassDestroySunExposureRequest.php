<?php

namespace App\Http\Requests;

use App\Models\SunExposure;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroySunExposureRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('sun_exposure_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:sun_exposures,id',
        ];
    }
}
