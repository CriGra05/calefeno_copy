<?php

namespace App\Http\Requests;

use App\Models\FlowerImage;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateFlowerImageRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('flower_image_edit');
    }

    public function rules()
    {
        return [];
    }
}
