<?php

namespace App\Http\Requests;

use App\Models\BloomEndPeriod;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyBloomEndPeriodRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('bloom_end_period_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:bloom_end_periods,id',
        ];
    }
}
