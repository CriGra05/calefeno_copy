<?php

namespace App\Http\Requests;

use App\Models\FlowerImage;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyFlowerImageRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('flower_image_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:flower_images,id',
        ];
    }
}
