<?php

namespace App\Http\Requests;

use App\Models\Slope;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreSlopeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('slope_create');
    }

    public function rules()
    {
        return [
            'terrain_slope' => [
                'string',
                'nullable',
            ],
        ];
    }
}
