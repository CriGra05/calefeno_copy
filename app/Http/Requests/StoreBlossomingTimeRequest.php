<?php

namespace App\Http\Requests;

use App\Models\BlossomingTime;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreBlossomingTimeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('blossoming_time_create');
    }

    public function rules()
    {
        return [
            'name_blossoming_time' => [
                'string',
                'required',
                'unique:blossoming_time',
            ],
            'notes' => [
                'string',
                'nullable',
            ],
        ];
    }
}
