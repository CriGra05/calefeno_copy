<?php

namespace App\Http\Requests;

use App\Models\PlantImage;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePlantImageRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('plant_image_create');
    }

    public function rules()
    {
        return [];
    }
}
