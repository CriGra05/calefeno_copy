<?php

namespace App\Http\Requests;

use App\Models\PhenologicalObservation;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePhenologicalObservationRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('phenological_observation_create');
    }

    public function rules()
    {
        return [
            'notes' => [
                'string',
                'nullable',
            ],
            'weather_conditions' => [
                'string',
                'nullable',
            ],
        ];
    }
}
