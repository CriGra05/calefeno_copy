<?php

namespace App\Http\Requests;

use App\Models\BloomEndPeriod;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreBloomEndPeriodRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('bloom_end_period_create');
    }

    public function rules()
    {
        return [
            'bloom_end_period' => [
                'string',
                'nullable',
            ],
            'observation' => [
                'string',
                'nullable',
            ],
        ];
    }
}
