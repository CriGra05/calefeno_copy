<?php

namespace App\Http\Requests;

use App\Models\Sorrounding;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreSorroundingRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('sorrounding_create');
    }

    public function rules()
    {
        return [
            'sorroundings' => [
                'string',
                'nullable',
            ],
            'description' => [
                'string',
                'nullable',
            ],
        ];
    }
}
