<?php

namespace App\Http\Requests;

use App\Models\FlowerImage;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreFlowerImageRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('flower_image_create');
    }

    public function rules()
    {
        return [];
    }
}
