<?php

namespace App\Http\Requests;

use App\Models\PlantImage;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyPlantImageRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('plant_image_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:plant_images,id',
        ];
    }
}
