<?php

namespace App\Http\Requests;

use App\Models\BlossomingTime;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyBlossomingTimeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('blossoming_time_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:blossoming_time,id',
        ];
    }
}
