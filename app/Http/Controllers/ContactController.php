<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;


class ContactController extends Controller
{
    public function showForm()
    {
        return view('contact.form');
    }

    public function submitForm(Request $request)
    {
        // Validate the form data
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        // Create a new instance of the Message model
        $message = new Message();
        $message->name = $request->name;
        $message->email = $request->email;
        $message->message = $request->message;
        $message->save();

        // Optionally, you can also use the create method to create and save the message in one line:
        // $message = Message::create($validatedData);


        // Process the form data, e.g., send an email, save to database, etc.

        // Optionally, you can redirect the user to a thank you page
        return redirect()->route('contact.thankyou');
    }

    // Other methods of the ContactController...

    public function thankYou()
    {
        return view('contact.thankyou');
    }

}

