<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Role;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //Redirect to Dashboard
    //protected $redirectTo = RouteServiceProvider::HOME;
    //Redirect to Homepage
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     * The constructor method __construct() sets the guest middleware,
     * which ensures that only guest users can access the registration page and process registration.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     * The validator() method defines the validation rules for the incoming registration request.
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'phone_number' => 'Il :attribute deve essere nel formato 0915554433.',
            'mobile_number' => 'Il :attribute deve essere nel formato 0795554433.',
            'email.email' => ':attribute deve essere nel formato tuamail@esempio.com.',
        ];

        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname'  => ['required', 'string', 'max:255'],
            'address'   => ['string', 'max:255'],
            'postcode'  => ['required', 'digits:4'],
            'place'     => ['required', 'string', 'max:255', 'regex:/^[A-Za-z\s]+$/'],
            'phone'     => ['nullable', 'string', 'max:50', 'phone_number'],
            'mobile'    => ['nullable', 'string', 'max:50', 'mobile_number'],
            'email'     => ['required', 'string', 'email:filter', 'max:255', 'unique:users'],
            'password'  => ['required', 'string', 'min:8', 'confirmed'],
        ], $messages);
    }

         /**
          * Create a new user instance after a valid registration.
          *
          * @param  array  $data
          * @return \App\User
          */
         protected function create(array $data)
         {
            $user = User::create([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'address' => $data['address'],
                'postcode' => $data['postcode'],
                'place' => $data['place'],
                'phone' => $data['phone'],
                'mobile' => $data['mobile'],
                'email'    => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => Role::where('title', 'user')->first()->id
            ]);

            // Retrieve the "user" role
            //$userRole = Role::where('name', 'user')->first();

            // Assign the role to the newly registered user
            //$user->roles()->attach($userRole);

            return $user;
            //  return User::create([
            //      'firstname' => $data['firstname'],
            //      'lastname' => $data['lastname'],
            //      'address' => $data['address'],
            //      'postcode' => $data['postcode'],
            //      'place' => $data['place'],
            //      'phone' => $data['phone'],
            //      'mobile' => $data['mobile'],
            //      'email'    => $data['email'],
            //      'password' => Hash::make($data['password']),
            //  ]);

                 // Retrieve the "user" role
            //$userRole = Role::where('name', 'user')->first();

            // Assign the role to the newly registered user
            //$user->roles()->attach($userRole);
         }
}
