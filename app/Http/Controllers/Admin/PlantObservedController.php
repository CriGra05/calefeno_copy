<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPlantObservedRequest;
use App\Http\Requests\StorePlantObservedRequest;
use App\Http\Requests\UpdatePlantObservedRequest;
use App\Models\ObservationZone;
use App\Models\Plant;
use App\Models\PlantObserved;
use App\Models\Slope;
use App\Models\Sorrounding;
use App\Models\SunExposure;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PlantObservedController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('plant_observed_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plantObserveds = PlantObserved::with(['sun_exposure', 'slope', 'sorroundings', 'plant_name', 'observed_zone'])->get();

        return view('admin.plantObserveds.index', compact('plantObserveds'));
    }

    public function create()
    {
        abort_if(Gate::denies('plant_observed_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sun_exposures = SunExposure::pluck('sun_exposure', 'id')->prepend(trans('global.pleaseSelect'), '');

        $slopes = Slope::pluck('terrain_slope', 'id')->prepend(trans('global.pleaseSelect'), '');

        $sorroundings = Sorrounding::pluck('sorroundings', 'id')->prepend(trans('global.pleaseSelect'), '');

        $plant_names = Plant::pluck('plant_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $observed_zones = ObservationZone::pluck('zone_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.plantObserveds.create', compact('observed_zones', 'plant_names', 'slopes', 'sorroundings', 'sun_exposures'));
    }

    public function store(StorePlantObservedRequest $request)
    {
        $plantObserved = PlantObserved::create($request->all());

        return redirect()->route('admin.plant-observeds.index');
    }

    public function edit(PlantObserved $plantObserved)
    {
        abort_if(Gate::denies('plant_observed_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sun_exposures = SunExposure::pluck('sun_exposure', 'id')->prepend(trans('global.pleaseSelect'), '');

        $slopes = Slope::pluck('terrain_slope', 'id')->prepend(trans('global.pleaseSelect'), '');

        $sorroundings = Sorrounding::pluck('sorroundings', 'id')->prepend(trans('global.pleaseSelect'), '');

        $plant_names = Plant::pluck('plant_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $observed_zones = ObservationZone::pluck('zone_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $plantObserved->load('sun_exposure', 'slope', 'sorroundings', 'plant_name', 'observed_zone');

        return view('admin.plantObserveds.edit', compact('observed_zones', 'plantObserved', 'plant_names', 'slopes', 'sorroundings', 'sun_exposures'));
    }

    public function update(UpdatePlantObservedRequest $request, PlantObserved $plantObserved)
    {
        $plantObserved->update($request->all());

        return redirect()->route('admin.plant-observeds.index');
    }

    public function show(PlantObserved $plantObserved)
    {
        abort_if(Gate::denies('plant_observed_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plantObserved->load('sun_exposure', 'slope', 'sorroundings', 'plant_name', 'observed_zone');

        return view('admin.plantObserveds.show', compact('plantObserved'));
    }

    public function destroy(PlantObserved $plantObserved)
    {
        abort_if(Gate::denies('plant_observed_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plantObserved->delete();

        return back();
    }

    public function massDestroy(MassDestroyPlantObservedRequest $request)
    {
        $plantObserveds = PlantObserved::find(request('ids'));

        foreach ($plantObserveds as $plantObserved) {
            $plantObserved->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
