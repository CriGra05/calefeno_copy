<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBloomStartPeriodRequest;
use App\Http\Requests\StoreBloomStartPeriodRequest;
use App\Http\Requests\UpdateBloomStartPeriodRequest;
use App\Models\BloomStartPeriod;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BloomStartPeriodController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('bloom_start_period_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bloomStartPeriods = BloomStartPeriod::all();

        return view('admin.bloomStartPeriods.index', compact('bloomStartPeriods'));
    }

    public function create()
    {
        abort_if(Gate::denies('bloom_start_period_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bloomStartPeriods.create');
    }

    public function store(StoreBloomStartPeriodRequest $request)
    {
        $bloomStartPeriod = BloomStartPeriod::create($request->all());

        return redirect()->route('admin.bloom-start-periods.index');
    }

    public function edit(BloomStartPeriod $bloomStartPeriod)
    {
        abort_if(Gate::denies('bloom_start_period_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bloomStartPeriods.edit', compact('bloomStartPeriod'));
    }

    public function update(UpdateBloomStartPeriodRequest $request, BloomStartPeriod $bloomStartPeriod)
    {
        $bloomStartPeriod->update($request->all());

        return redirect()->route('admin.bloom-start-periods.index');
    }

    public function show(BloomStartPeriod $bloomStartPeriod)
    {
        abort_if(Gate::denies('bloom_start_period_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bloomStartPeriods.show', compact('bloomStartPeriod'));
    }

    public function destroy(BloomStartPeriod $bloomStartPeriod)
    {
        abort_if(Gate::denies('bloom_start_period_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bloomStartPeriod->delete();

        return back();
    }

    public function massDestroy(MassDestroyBloomStartPeriodRequest $request)
    {
        $bloomStartPeriods = BloomStartPeriod::find(request('ids'));

        foreach ($bloomStartPeriods as $bloomStartPeriod) {
            $bloomStartPeriod->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
