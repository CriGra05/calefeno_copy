<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyFlowerImageRequest;
use App\Http\Requests\StoreFlowerImageRequest;
use App\Http\Requests\UpdateFlowerImageRequest;
use App\Models\FlowerImage;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class FlowerImageController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('flower_image_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $flowerImages = FlowerImage::with(['media'])->get();

        return view('admin.flowerImages.index', compact('flowerImages'));
    }

    public function create()
    {
        abort_if(Gate::denies('flower_image_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.flowerImages.create');
    }

    public function store(StoreFlowerImageRequest $request)
    {
        $flowerImage = FlowerImage::create($request->all());

        if ($request->input('flower_pic', false)) {
            $flowerImage->addMedia(storage_path('tmp/uploads/' . basename($request->input('flower_pic'))))->toMediaCollection('flower_pic');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $flowerImage->id]);
        }

        return redirect()->route('admin.flower-images.index');
    }

    public function edit(FlowerImage $flowerImage)
    {
        abort_if(Gate::denies('flower_image_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.flowerImages.edit', compact('flowerImage'));
    }

    public function update(UpdateFlowerImageRequest $request, FlowerImage $flowerImage)
    {
        $flowerImage->update($request->all());

        if ($request->input('flower_pic', false)) {
            if (! $flowerImage->flower_pic || $request->input('flower_pic') !== $flowerImage->flower_pic->file_name) {
                if ($flowerImage->flower_pic) {
                    $flowerImage->flower_pic->delete();
                }
                $flowerImage->addMedia(storage_path('tmp/uploads/' . basename($request->input('flower_pic'))))->toMediaCollection('flower_pic');
            }
        } elseif ($flowerImage->flower_pic) {
            $flowerImage->flower_pic->delete();
        }

        return redirect()->route('admin.flower-images.index');
    }

    public function show(FlowerImage $flowerImage)
    {
        abort_if(Gate::denies('flower_image_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.flowerImages.show', compact('flowerImage'));
    }

    public function destroy(FlowerImage $flowerImage)
    {
        abort_if(Gate::denies('flower_image_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $flowerImage->delete();

        return back();
    }

    public function massDestroy(MassDestroyFlowerImageRequest $request)
    {
        $flowerImages = FlowerImage::find(request('ids'));

        foreach ($flowerImages as $flowerImage) {
            $flowerImage->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('flower_image_create') && Gate::denies('flower_image_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new FlowerImage();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
