<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySunExposureRequest;
use App\Http\Requests\StoreSunExposureRequest;
use App\Http\Requests\UpdateSunExposureRequest;
use App\Models\SunExposure;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SunExposureController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('sun_exposure_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sunExposures = SunExposure::all();

        return view('admin.sunExposures.index', compact('sunExposures'));
    }

    public function create()
    {
        abort_if(Gate::denies('sun_exposure_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sunExposures.create');
    }

    public function store(StoreSunExposureRequest $request)
    {
        $sunExposure = SunExposure::create($request->all());

        return redirect()->route('admin.sun-exposures.index');
    }

    public function edit(SunExposure $sunExposure)
    {
        abort_if(Gate::denies('sun_exposure_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sunExposures.edit', compact('sunExposure'));
    }

    public function update(UpdateSunExposureRequest $request, SunExposure $sunExposure)
    {
        $sunExposure->update($request->all());

        return redirect()->route('admin.sun-exposures.index');
    }

    public function show(SunExposure $sunExposure)
    {
        abort_if(Gate::denies('sun_exposure_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sunExposures.show', compact('sunExposure'));
    }

    public function destroy(SunExposure $sunExposure)
    {
        abort_if(Gate::denies('sun_exposure_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sunExposure->delete();

        return back();
    }

    public function massDestroy(MassDestroySunExposureRequest $request)
    {
        $sunExposures = SunExposure::find(request('ids'));

        foreach ($sunExposures as $sunExposure) {
            $sunExposure->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
