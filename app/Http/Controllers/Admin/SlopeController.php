<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySlopeRequest;
use App\Http\Requests\StoreSlopeRequest;
use App\Http\Requests\UpdateSlopeRequest;
use App\Models\Slope;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SlopeController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('slope_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $slopes = Slope::all();

        return view('admin.slopes.index', compact('slopes'));
    }

    public function create()
    {
        abort_if(Gate::denies('slope_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.slopes.create');
    }

    public function store(StoreSlopeRequest $request)
    {
        $slope = Slope::create($request->all());

        return redirect()->route('admin.slopes.index');
    }

    public function edit(Slope $slope)
    {
        abort_if(Gate::denies('slope_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.slopes.edit', compact('slope'));
    }

    public function update(UpdateSlopeRequest $request, Slope $slope)
    {
        $slope->update($request->all());

        return redirect()->route('admin.slopes.index');
    }

    public function show(Slope $slope)
    {
        abort_if(Gate::denies('slope_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.slopes.show', compact('slope'));
    }

    public function destroy(Slope $slope)
    {
        abort_if(Gate::denies('slope_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $slope->delete();

        return back();
    }

    public function massDestroy(MassDestroySlopeRequest $request)
    {
        $slopes = Slope::find(request('ids'));

        foreach ($slopes as $slope) {
            $slope->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
