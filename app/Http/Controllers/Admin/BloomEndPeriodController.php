<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBloomEndPeriodRequest;
use App\Http\Requests\StoreBloomEndPeriodRequest;
use App\Http\Requests\UpdateBloomEndPeriodRequest;
use App\Models\BloomEndPeriod;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BloomEndPeriodController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('bloom_end_period_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bloomEndPeriods = BloomEndPeriod::all();

        return view('admin.bloomEndPeriods.index', compact('bloomEndPeriods'));
    }

    public function create()
    {
        abort_if(Gate::denies('bloom_end_period_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bloomEndPeriods.create');
    }

    public function store(StoreBloomEndPeriodRequest $request)
    {
        $bloomEndPeriod = BloomEndPeriod::create($request->all());

        return redirect()->route('admin.bloom-end-periods.index');
    }

    public function edit(BloomEndPeriod $bloomEndPeriod)
    {
        abort_if(Gate::denies('bloom_end_period_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bloomEndPeriods.edit', compact('bloomEndPeriod'));
    }

    public function update(UpdateBloomEndPeriodRequest $request, BloomEndPeriod $bloomEndPeriod)
    {
        $bloomEndPeriod->update($request->all());

        return redirect()->route('admin.bloom-end-periods.index');
    }

    public function show(BloomEndPeriod $bloomEndPeriod)
    {
        abort_if(Gate::denies('bloom_end_period_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bloomEndPeriods.show', compact('bloomEndPeriod'));
    }

    public function destroy(BloomEndPeriod $bloomEndPeriod)
    {
        abort_if(Gate::denies('bloom_end_period_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bloomEndPeriod->delete();

        return back();
    }

    public function massDestroy(MassDestroyBloomEndPeriodRequest $request)
    {
        $bloomEndPeriods = BloomEndPeriod::find(request('ids'));

        foreach ($bloomEndPeriods as $bloomEndPeriod) {
            $bloomEndPeriod->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
