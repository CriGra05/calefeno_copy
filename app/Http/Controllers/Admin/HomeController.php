<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;

class HomeController
{
    public function index()
    {
        $u=Auth::user()->name;
        return view('home', compact('u'));
    }
}
