<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBlossomingTimeRequest;
use App\Http\Requests\StoreBlossomingTimeRequest;
use App\Http\Requests\UpdateBlossomingTimeRequest;
use App\Models\BlossomingTime;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BlossomingTimeController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('blossoming_time_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $blossomingTimes = BlossomingTime::all();

        return view('admin.blossomingTimes.index', compact('blossomingTimes'));
    }

    public function create()
    {
        abort_if(Gate::denies('blossoming_time_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.blossomingTimes.create');
    }

    public function store(StoreBlossomingTimeRequest $request)
    {
        $blossomingTime = BlossomingTime::create($request->all());

        return redirect()->route('admin.blossoming-times.index');
    }

    public function edit(BlossomingTime $blossomingTime)
    {
        abort_if(Gate::denies('blossoming_time_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.blossomingTimes.edit', compact('blossomingTime'));
    }

    public function update(UpdateBlossomingTimeRequest $request, BlossomingTime $blossomingTime)
    {
        $blossomingTime->update($request->all());

        return redirect()->route('admin.blossoming-times.index');
    }

    public function show(BlossomingTime $blossomingTime)
    {
        abort_if(Gate::denies('blossoming_time_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.blossomingTimes.show', compact('blossomingTime'));
    }

    public function destroy(BlossomingTime $blossomingTime)
    {
        abort_if(Gate::denies('blossoming_time_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $blossomingTime->delete();

        return back();
    }

    public function massDestroy(MassDestroyBlossomingTimeRequest $request)
    {
        $blossomingTimes = BlossomingTime::find(request('ids'));

        foreach ($blossomingTimes as $blossomingTime) {
            $blossomingTime->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
