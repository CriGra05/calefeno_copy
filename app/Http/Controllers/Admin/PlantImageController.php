<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyPlantImageRequest;
use App\Http\Requests\StorePlantImageRequest;
use App\Http\Requests\UpdatePlantImageRequest;
use App\Models\PlantImage;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class PlantImageController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('plant_image_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plantImages = PlantImage::with(['media'])->get();

        return view('admin.plantImages.index', compact('plantImages'));
    }

    public function create()
    {
        abort_if(Gate::denies('plant_image_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.plantImages.create');
    }

    public function store(StorePlantImageRequest $request)
    {
        $plantImage = PlantImage::create($request->all());

        if ($request->input('plant_pic', false)) {
            $plantImage->addMedia(storage_path('tmp/uploads/' . basename($request->input('plant_pic'))))->toMediaCollection('plant_pic');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $plantImage->id]);
        }

        return redirect()->route('admin.plant-images.index');
    }

    public function edit(PlantImage $plantImage)
    {
        abort_if(Gate::denies('plant_image_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.plantImages.edit', compact('plantImage'));
    }

    public function update(UpdatePlantImageRequest $request, PlantImage $plantImage)
    {
        $plantImage->update($request->all());

        if ($request->input('plant_pic', false)) {
            if (! $plantImage->plant_pic || $request->input('plant_pic') !== $plantImage->plant_pic->file_name) {
                if ($plantImage->plant_pic) {
                    $plantImage->plant_pic->delete();
                }
                $plantImage->addMedia(storage_path('tmp/uploads/' . basename($request->input('plant_pic'))))->toMediaCollection('plant_pic');
            }
        } elseif ($plantImage->plant_pic) {
            $plantImage->plant_pic->delete();
        }

        return redirect()->route('admin.plant-images.index');
    }

    public function show(PlantImage $plantImage)
    {
        abort_if(Gate::denies('plant_image_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.plantImages.show', compact('plantImage'));
    }

    public function destroy(PlantImage $plantImage)
    {
        abort_if(Gate::denies('plant_image_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plantImage->delete();

        return back();
    }

    public function massDestroy(MassDestroyPlantImageRequest $request)
    {
        $plantImages = PlantImage::find(request('ids'));

        foreach ($plantImages as $plantImage) {
            $plantImage->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('plant_image_create') && Gate::denies('plant_image_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new PlantImage();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
