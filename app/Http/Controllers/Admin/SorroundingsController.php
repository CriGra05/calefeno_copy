<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySorroundingRequest;
use App\Http\Requests\StoreSorroundingRequest;
use App\Http\Requests\UpdateSorroundingRequest;
use App\Models\Sorrounding;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SorroundingsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('sorrounding_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sorroundings = Sorrounding::all();

        return view('admin.sorroundings.index', compact('sorroundings'));
    }

    public function create()
    {
        abort_if(Gate::denies('sorrounding_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sorroundings.create');
    }

    public function store(StoreSorroundingRequest $request)
    {
        $sorrounding = Sorrounding::create($request->all());

        return redirect()->route('admin.sorroundings.index');
    }

    public function edit(Sorrounding $sorrounding)
    {
        abort_if(Gate::denies('sorrounding_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sorroundings.edit', compact('sorrounding'));
    }

    public function update(UpdateSorroundingRequest $request, Sorrounding $sorrounding)
    {
        $sorrounding->update($request->all());

        return redirect()->route('admin.sorroundings.index');
    }

    public function show(Sorrounding $sorrounding)
    {
        abort_if(Gate::denies('sorrounding_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sorroundings.show', compact('sorrounding'));
    }

    public function destroy(Sorrounding $sorrounding)
    {
        abort_if(Gate::denies('sorrounding_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sorrounding->delete();

        return back();
    }

    public function massDestroy(MassDestroySorroundingRequest $request)
    {
        $sorroundings = Sorrounding::find(request('ids'));

        foreach ($sorroundings as $sorrounding) {
            $sorrounding->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
