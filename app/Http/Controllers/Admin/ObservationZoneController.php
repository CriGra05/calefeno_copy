<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyObservationZoneRequest;
use App\Http\Requests\StoreObservationZoneRequest;
use App\Http\Requests\UpdateObservationZoneRequest;
use App\Models\ObservationZone;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ObservationZoneController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('observation_zone_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $observationZones = ObservationZone::all();

        return view('admin.observationZones.index', compact('observationZones'));
    }

    public function create()
    {
        abort_if(Gate::denies('observation_zone_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.observationZones.create');
    }

    public function store(StoreObservationZoneRequest $request)
    {
        $observationZone = ObservationZone::create($request->all());

        return redirect()->route('admin.observation-zones.index');
    }

    public function edit(ObservationZone $observationZone)
    {
        abort_if(Gate::denies('observation_zone_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.observationZones.edit', compact('observationZone'));
    }

    public function update(UpdateObservationZoneRequest $request, ObservationZone $observationZone)
    {
        $observationZone->update($request->all());

        return redirect()->route('admin.observation-zones.index');
    }

    public function show(ObservationZone $observationZone)
    {
        abort_if(Gate::denies('observation_zone_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $observationZone->load('observedZonePlantObserveds');

        return view('admin.observationZones.show', compact('observationZone'));
    }

    public function destroy(ObservationZone $observationZone)
    {
        abort_if(Gate::denies('observation_zone_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $observationZone->delete();

        return back();
    }

    public function massDestroy(MassDestroyObservationZoneRequest $request)
    {
        $observationZones = ObservationZone::find(request('ids'));

        foreach ($observationZones as $observationZone) {
            $observationZone->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
