<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPhenologicalObservationRequest;
use App\Http\Requests\StorePhenologicalObservationRequest;
use App\Http\Requests\UpdatePhenologicalObservationRequest;
use App\Models\BlossomingTime;
use App\Models\PhenologicalObservation;
//modifica: importo il modello di "Pianta osservata"
use App\Models\PlantObserved;

use Illuminate\Support\Facades\Auth;

use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;



class PhenologicalObservationController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('phenological_observation_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $phenologicalObservations = PhenologicalObservation::with(['blossoming_time'])->get();

        return view('admin.phenologicalObservations.index', compact('phenologicalObservations'));
    }

    public function create()
    {
        abort_if(Gate::denies('phenological_observation_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plantObserveds = PlantObserved::all();

        $blossoming_time = BlossomingTime::pluck('name_blossoming_time', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.phenologicalObservations.create', compact('blossoming_time','plantObserveds'));
    }

    public function store(StorePhenologicalObservationRequest $request)
    {
        $requestData = $request->all();
        $requestData['user_id'] = Auth::id(); // Set the user ID

        $phenologicalObservation = PhenologicalObservation::create($requestData);

        return redirect()->route('admin.phenological-observations.index');
    }


    // public function store(StorePhenologicalObservationRequest $request)
    // {
    //     $phenologicalObservation = PhenologicalObservation::create($request->all());

    //     return redirect()->route('admin.phenological-observations.index');
    // }

    // public function store(StorePhenologicalObservationRequest $request)
    // {
    //     //dd($request->all());
    //     // $phenologicalObservation = PhenologicalObservation::create($request->all());
    //     $phenologicalObservation = new PhenologicalObservation();
    //     $phenologicalObservation->fill($request->all());
    //     $phenologicalObservation->blossoming_time_id = $request->blossoming_time_id; // campo con "s" o senza "s", andrebbe armonizzato
    //     $phenologicalObservation->user_id =Auth::user()->id;
    //     //$phenologicalObservation->plant_observed_id = ??? // Campo da aggiungere o migration da cambiare
    //     $phenologicalObservation->save();

    //     return redirect()->route('admin.phenological-observations.index');
    // }


    public function edit(PhenologicalObservation $phenologicalObservation)
    {
        abort_if(Gate::denies('phenological_observation_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $blossoming_time = BlossomingTime::pluck('name_blossoming_time', 'id')->prepend(trans('global.pleaseSelect'), '');

        $phenologicalObservation->load('blossoming_time');

        return view('admin.phenologicalObservations.edit', compact('blossoming_time', 'phenologicalObservation'));
    }

    public function update(UpdatePhenologicalObservationRequest $request, PhenologicalObservation $phenologicalObservation)
    {
        $phenologicalObservation->update($request->all());

        return redirect()->route('admin.phenological-observations.index');
    }

    /* public function show(PhenologicalObservation $phenologicalObservation)
    {
        abort_if(Gate::denies('phenological_observation_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $phenologicalObservation->load('blossoming_time');

        return view('admin.phenologicalObservations.show', compact('phenologicalObservation'));
    }*/
    //viene aggiunto il caricamento della tabella 'plantObserved'
    public function show(PhenologicalObservation $phenologicalObservation)
    {
        abort_if(Gate::denies('phenological_observation_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $phenologicalObservation->load('blossoming_time', 'plantObserved');

        return view('admin.phenologicalObservations.show', compact('phenologicalObservation'));
    }


    public function destroy(PhenologicalObservation $phenologicalObservation)
    {
        abort_if(Gate::denies('phenological_observation_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $phenologicalObservation->delete();

        return back();
    }

    public function massDestroy(MassDestroyPhenologicalObservationRequest $request)
    {
        $phenologicalObservations = PhenologicalObservation::find(request('ids'));

        foreach ($phenologicalObservations as $phenologicalObservation) {
            $phenologicalObservation->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }

}
