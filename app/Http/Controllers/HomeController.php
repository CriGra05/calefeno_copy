<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;


use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $users=User::all();


        return view('welcome', compact('users'));


    }
}
