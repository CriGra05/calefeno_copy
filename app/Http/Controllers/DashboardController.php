<?php

namespace App\Http\Controllers;

use App\Models\PhenologicalObservation;
use App\Models\PlantObserved;

class DashboardController extends Controller
{
    public function index()
    {
        // Retrieve the statistics data
        $totalPlantsObserved = PlantObserved::count();
        $totalPhenologicalObservations = PhenologicalObservation::count();



        // Pass the data to the view
        return view('home', compact('totalPlantsObserved', 'totalPhenologicalObservations'));
    }
}

