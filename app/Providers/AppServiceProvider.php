<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Validator::extend('phone_number', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^0\d{9}$/', $value);
        });
        Validator::extend('mobile_number', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^0\d{9}$/', $value);
        });
    }

}
