<?php


//Route::redirect('/', '/login');
Route::get('/', 'HomeController@index')->name('homesite');

Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

/* By calling Auth::routes([]), Laravel automatically generates the authentication routes,
including the registration route, with the necessary controllers and methods.
The generated routes can be found in the routes/web.php file. */

//Route contact form and contact/thankyou view

Route::get('/contact', 'ContactController@showForm')->name('contact');
Route::post('/contact', 'ContactController@submitForm')->name('contact.submit');
Route::get('/contact/thankyou', 'ContactController@thankYou')->name('contact.thankyou');

// Authentication routes with email verification
Auth::routes(['verify' => true]);



Auth::routes([]);

//All Views routes

Route::view('/about', 'about')->name('about');

//Add Statistic in the home.blade.php
Route::get('/home', 'DashboardController@index');


Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Person
    Route::delete('people/destroy', 'PersonController@massDestroy')->name('people.massDestroy');
    Route::resource('people', 'PersonController');

    // Phenological Observation
    Route::delete('phenological-observations/destroy', 'PhenologicalObservationController@massDestroy')->name('phenological-observations.massDestroy');
    Route::resource('phenological-observations', 'PhenologicalObservationController');

    // Blossoming Time
    Route::delete('blossoming-times/destroy', 'BlossomingTimeController@massDestroy')->name('blossoming-times.massDestroy');
    Route::resource('blossoming-times', 'BlossomingTimeController');

    // Plant Observed
    Route::delete('plant-observeds/destroy', 'PlantObservedController@massDestroy')->name('plant-observeds.massDestroy');
    Route::resource('plant-observeds', 'PlantObservedController');

    // Sun Exposure
    Route::delete('sun-exposures/destroy', 'SunExposureController@massDestroy')->name('sun-exposures.massDestroy');
    Route::resource('sun-exposures', 'SunExposureController');

    // Slope
    Route::delete('slopes/destroy', 'SlopeController@massDestroy')->name('slopes.massDestroy');
    Route::resource('slopes', 'SlopeController');

    // Sorroundings
    Route::delete('sorroundings/destroy', 'SorroundingsController@massDestroy')->name('sorroundings.massDestroy');
    Route::resource('sorroundings', 'SorroundingsController');

    // Plant
    Route::delete('plants/destroy', 'PlantController@massDestroy')->name('plants.massDestroy');
    Route::resource('plants', 'PlantController');

    // Observation Zone
    Route::delete('observation-zones/destroy', 'ObservationZoneController@massDestroy')->name('observation-zones.massDestroy');
    Route::resource('observation-zones', 'ObservationZoneController');

    // Location
    Route::delete('locations/destroy', 'LocationController@massDestroy')->name('locations.massDestroy');
    Route::resource('locations', 'LocationController');

    // Plant Image
    Route::delete('plant-images/destroy', 'PlantImageController@massDestroy')->name('plant-images.massDestroy');
    Route::post('plant-images/media', 'PlantImageController@storeMedia')->name('plant-images.storeMedia');
    Route::post('plant-images/ckmedia', 'PlantImageController@storeCKEditorImages')->name('plant-images.storeCKEditorImages');
    Route::resource('plant-images', 'PlantImageController');

    // Flower Image
    Route::delete('flower-images/destroy', 'FlowerImageController@massDestroy')->name('flower-images.massDestroy');
    Route::post('flower-images/media', 'FlowerImageController@storeMedia')->name('flower-images.storeMedia');
    Route::post('flower-images/ckmedia', 'FlowerImageController@storeCKEditorImages')->name('flower-images.storeCKEditorImages');
    Route::resource('flower-images', 'FlowerImageController');

    // Bloom Start Period
    Route::delete('bloom-start-periods/destroy', 'BloomStartPeriodController@massDestroy')->name('bloom-start-periods.massDestroy');
    Route::resource('bloom-start-periods', 'BloomStartPeriodController');

    // Bloom End Period
    Route::delete('bloom-end-periods/destroy', 'BloomEndPeriodController@massDestroy')->name('bloom-end-periods.massDestroy');
    Route::resource('bloom-end-periods', 'BloomEndPeriodController');


});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});



