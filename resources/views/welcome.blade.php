@extends('layouts.app')

@section('content')


{{--<!DOCTYPE html>--}}
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}
{{--    <head>--}}
{{--        <meta charset="utf-8">--}}
{{--        <meta name="viewport" content="width=device-width, initial-scale=1">--}}

{{--        <title>Calendario fenologico</title>--}}

{{--        <!-- Fonts -->--}}
{{--        <link rel="preconnect" href="https://fonts.bunny.net">--}}
{{--        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />--}}
{{--        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">--}}

{{--        <!-- Styles -->--}}

{{--    </head>--}}
@section('content')

    <section class="row">

        <article class="col-md-4">
            <div class="card">
                <img src="{{url('/img/pollini.png')}}" alt="pollini" height="300px" class="card-img-top w-100 " >
                <div class="card-body">
                    <h5 class="card-title">Osservazioni fenologiche</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <a href="#" class="btn btn-secondary">Vedi statistiche</a>
                </div>
            </div>

        </article>
        <article class="col-md-4">
            <div class="card">
                <img src="{{url('/img/mappa.png')}}" alt="mappa topografica" height="300px"  class="card-img-top w-100">
                <div class="card-body">
                    <h5 class="card-title">Zone di osservazioni</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <a href="#" class="btn btn-secondary">Vedi mappa</a>
                </div>
            </div>

        </article>
        <article class="col-md-4">
            <div class="card">
                <img src="{{url('/img/arnie.png')}}" alt="Arnie" height="300px" class="card-img-top w-100">
                <div class="card-body">
                    <h5 class="card-title">Apicoltura</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <a href="#" class="btn btn-secondary">Vedi dettagli</a>
                </div>
            </div>

        </article>

        <section class="row md-5">
            <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quisquam iure repellendus, minus minima
                tempore at recusandae optio perspiciatis provident molestiae dolorem excepturi ullam, odit a
                facilis, doloribus ut delectus quam!
            </p>
            <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quisquam iure repellendus, minus minima
                tempore at recusandae optio perspiciatis provident molestiae dolorem excepturi ullam, odit a
                facilis, doloribus ut delectus quam!
            </p>
            <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quisquam iure repellendus, minus minima
                tempore at recusandae optio perspiciatis provident molestiae dolorem excepturi ullam, odit a
                facilis, doloribus ut delectus quam!
            </p>
        </section>

    </section>







@endsection

