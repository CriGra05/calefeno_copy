@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.phenologicalObservation.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.phenological-observations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologicalObservation.fields.id') }}
                        </th>
                        <td>
                            {{ $phenologicalObservation->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologicalObservation.fields.notes') }}
                        </th>
                        <td>
                            {{ $phenologicalObservation->notes }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologicalObservation.fields.weather_conditions') }}
                        </th>
                        <td>
                            {{ $phenologicalObservation->weather_conditions }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologicalObservation.fields.blossoming_time') }}
                        </th>
                        <td>
                            {{ $phenologicalObservation->blossoming_time->name_blossoming_time ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.notes') }}
                        </th>
                        <td>
                            {{ $phenologicalObservation->plantObserved->notes ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.altitude') }}
                        </th>
                        <td>
                            {{ $phenologicalObservation->plantObserved->altitude ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.coordinates') }}
                        </th>
                        <td>
                            {{ $phenologicalObservation->plantObserved->coordinates ?? '' }}
                        </td>
                    </tr>

                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.phenological-observations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endsection
