@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.phenologicalObservation.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.phenological-observations.store") }}" enctype="multipart/form-data">
            @csrf
            <!--aggiunta campo nome pianta osservata-->
            <div class="form-group">
                <label for="plant_observed_id">{{ trans('cruds.phenologicalObservation.fields.plant_observed_name') }}</label>
                <select class="form-control select2 {{ $errors->has('plant_observed_id') ? 'is-invalid' : '' }}" name="plant_observed_id" id="plant_observed_id">
                    <option value="" selected disabled>{{ trans('global.pleaseSelect') }}</option>
                    @foreach($plantObserveds as $id => $plantObserved)
                        <option value="{{ $id }}">{{ $plantObserved->plant_observed_name }}</option>
                    @endforeach
                </select>
                @if($errors->has('plant_observed_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('plant_observed_id') }}
                    </div>
                @endif
            </div>
            <!--fine aggiunta-->
            <div class="form-group">
                <label for="blossoming_time_id">{{ trans('cruds.phenologicalObservation.fields.blossoming_time') }}</label>
                <select class="form-control select2 {{ $errors->has('blossoming_time') ? 'is-invalid' : '' }}" name="blossoming_time_id" id="blossoming_time_id">
                    @foreach($blossoming_time as $id => $entry)
                        <option value="{{ $id }}" {{ old('blossoming_time_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('blossoming_time'))
                    <div class="invalid-feedback">
                        {{ $errors->first('blossoming_time') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.phenologicalObservation.fields.blossoming_time_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="weather_conditions">{{ trans('cruds.phenologicalObservation.fields.weather_conditions') }}</label>
                <input class="form-control {{ $errors->has('weather_conditions') ? 'is-invalid' : '' }}" type="text" name="weather_conditions" id="weather_conditions" value="{{ old('weather_conditions', '') }}">
                @if($errors->has('weather_conditions'))
                    <div class="invalid-feedback">
                        {{ $errors->first('weather_conditions') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.phenologicalObservation.fields.weather_conditions_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="notes">{{ trans('cruds.phenologicalObservation.fields.notes') }}</label>
                <input class="form-control {{ $errors->has('notes') ? 'is-invalid' : '' }}" type="text" name="notes" id="notes" value="{{ old('notes', '') }}">
                @if($errors->has('notes'))
                    <div class="invalid-feedback">
                        {{ $errors->first('notes') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.phenologicalObservation.fields.notes_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
