@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.bloomEndPeriod.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.bloom-end-periods.update", [$bloomEndPeriod->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="bloom_end_period">{{ trans('cruds.bloomEndPeriod.fields.bloom_end_period') }}</label>
                <input class="form-control {{ $errors->has('bloom_end_period') ? 'is-invalid' : '' }}" type="text" name="bloom_end_period" id="bloom_end_period" value="{{ old('bloom_end_period', $bloomEndPeriod->bloom_end_period) }}">
                @if($errors->has('bloom_end_period'))
                    <div class="invalid-feedback">
                        {{ $errors->first('bloom_end_period') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.bloomEndPeriod.fields.bloom_end_period_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="observation">{{ trans('cruds.bloomEndPeriod.fields.observation') }}</label>
                <input class="form-control {{ $errors->has('observation') ? 'is-invalid' : '' }}" type="text" name="observation" id="observation" value="{{ old('observation', $bloomEndPeriod->observation) }}">
                @if($errors->has('observation'))
                    <div class="invalid-feedback">
                        {{ $errors->first('observation') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.bloomEndPeriod.fields.observation_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection