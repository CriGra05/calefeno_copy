@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.bloomEndPeriod.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bloom-end-periods.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.bloomEndPeriod.fields.id') }}
                        </th>
                        <td>
                            {{ $bloomEndPeriod->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bloomEndPeriod.fields.bloom_end_period') }}
                        </th>
                        <td>
                            {{ $bloomEndPeriod->bloom_end_period }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bloomEndPeriod.fields.observation') }}
                        </th>
                        <td>
                            {{ $bloomEndPeriod->observation }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bloom-end-periods.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection