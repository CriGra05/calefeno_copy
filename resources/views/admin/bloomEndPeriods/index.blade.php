@extends('layouts.admin')
@section('content')
@can('bloom_end_period_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.bloom-end-periods.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.bloomEndPeriod.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.bloomEndPeriod.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-BloomEndPeriod">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.bloomEndPeriod.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.bloomEndPeriod.fields.bloom_end_period') }}
                        </th>
                        <th>
                            {{ trans('cruds.bloomEndPeriod.fields.observation') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bloomEndPeriods as $key => $bloomEndPeriod)
                        <tr data-entry-id="{{ $bloomEndPeriod->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $bloomEndPeriod->id ?? '' }}
                            </td>
                            <td>
                                {{ $bloomEndPeriod->bloom_end_period ?? '' }}
                            </td>
                            <td>
                                {{ $bloomEndPeriod->observation ?? '' }}
                            </td>
                            <td>
                                @can('bloom_end_period_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.bloom-end-periods.show', $bloomEndPeriod->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('bloom_end_period_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.bloom-end-periods.edit', $bloomEndPeriod->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('bloom_end_period_delete')
                                    <form action="{{ route('admin.bloom-end-periods.destroy', $bloomEndPeriod->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('bloom_end_period_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.bloom-end-periods.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-BloomEndPeriod:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection