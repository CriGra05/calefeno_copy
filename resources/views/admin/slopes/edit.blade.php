@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.slope.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.slopes.update", [$slope->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="terrain_slope">{{ trans('cruds.slope.fields.terrain_slope') }}</label>
                <input class="form-control {{ $errors->has('terrain_slope') ? 'is-invalid' : '' }}" type="text" name="terrain_slope" id="terrain_slope" value="{{ old('terrain_slope', $slope->terrain_slope) }}">
                @if($errors->has('terrain_slope'))
                    <div class="invalid-feedback">
                        {{ $errors->first('terrain_slope') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.slope.fields.terrain_slope_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection