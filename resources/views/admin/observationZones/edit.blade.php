@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.observationZone.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.observation-zones.update", [$observationZone->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="zone_name">{{ trans('cruds.observationZone.fields.zone_name') }}</label>
                <input class="form-control {{ $errors->has('zone_name') ? 'is-invalid' : '' }}" type="text" name="zone_name" id="zone_name" value="{{ old('zone_name', $observationZone->zone_name) }}">
                @if($errors->has('zone_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('zone_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.observationZone.fields.zone_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="zip_code">{{ trans('cruds.observationZone.fields.zip_code') }}</label>
                <input class="form-control {{ $errors->has('zip_code') ? 'is-invalid' : '' }}" type="number" name="zip_code" id="zip_code" value="{{ old('zip_code', $observationZone->zip_code) }}" step="1">
                @if($errors->has('zip_code'))
                    <div class="invalid-feedback">
                        {{ $errors->first('zip_code') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.observationZone.fields.zip_code_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection