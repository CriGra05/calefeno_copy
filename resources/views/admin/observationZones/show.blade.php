@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.observationZone.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.observation-zones.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.observationZone.fields.id') }}
                        </th>
                        <td>
                            {{ $observationZone->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.observationZone.fields.zone_name') }}
                        </th>
                        <td>
                            {{ $observationZone->zone_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.observationZone.fields.zip_code') }}
                        </th>
                        <td>
                            {{ $observationZone->zip_code }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.observation-zones.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#observed_zone_plant_observeds" role="tab" data-toggle="tab">
                {{ trans('cruds.plantObserved.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="observed_zone_plant_observeds">
            @includeIf('admin.observationZones.relationships.observedZonePlantObserveds', ['plantObserveds' => $observationZone->observedZonePlantObserveds])
        </div>
    </div>
</div>

@endsection