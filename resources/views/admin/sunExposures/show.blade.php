@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.sunExposure.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.sun-exposures.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.sunExposure.fields.id') }}
                        </th>
                        <td>
                            {{ $sunExposure->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.sunExposure.fields.sun_exposure') }}
                        </th>
                        <td>
                            {{ $sunExposure->sun_exposure }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.sunExposure.fields.notes') }}
                        </th>
                        <td>
                            {{ $sunExposure->notes }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.sun-exposures.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection