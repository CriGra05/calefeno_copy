@extends('layouts.admin')
@section('content')
@can('sun_exposure_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.sun-exposures.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.sunExposure.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.sunExposure.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-SunExposure">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.sunExposure.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.sunExposure.fields.sun_exposure') }}
                        </th>
                        <th>
                            {{ trans('cruds.sunExposure.fields.notes') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sunExposures as $key => $sunExposure)
                        <tr data-entry-id="{{ $sunExposure->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $sunExposure->id ?? '' }}
                            </td>
                            <td>
                                {{ $sunExposure->sun_exposure ?? '' }}
                            </td>
                            <td>
                                {{ $sunExposure->notes ?? '' }}
                            </td>
                            <td>
                                @can('sun_exposure_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.sun-exposures.show', $sunExposure->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('sun_exposure_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.sun-exposures.edit', $sunExposure->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('sun_exposure_delete')
                                    <form action="{{ route('admin.sun-exposures.destroy', $sunExposure->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('sun_exposure_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.sun-exposures.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-SunExposure:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection