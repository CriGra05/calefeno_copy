@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.sunExposure.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.sun-exposures.update", [$sunExposure->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="sun_exposure">{{ trans('cruds.sunExposure.fields.sun_exposure') }}</label>
                <input class="form-control {{ $errors->has('sun_exposure') ? 'is-invalid' : '' }}" type="text" name="sun_exposure" id="sun_exposure" value="{{ old('sun_exposure', $sunExposure->sun_exposure) }}">
                @if($errors->has('sun_exposure'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sun_exposure') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.sunExposure.fields.sun_exposure_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="notes">{{ trans('cruds.sunExposure.fields.notes') }}</label>
                <input class="form-control {{ $errors->has('notes') ? 'is-invalid' : '' }}" type="text" name="notes" id="notes" value="{{ old('notes', $sunExposure->notes) }}">
                @if($errors->has('notes'))
                    <div class="invalid-feedback">
                        {{ $errors->first('notes') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.sunExposure.fields.notes_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection