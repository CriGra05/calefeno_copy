@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.bloomStartPeriod.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bloom-start-periods.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.bloomStartPeriod.fields.id') }}
                        </th>
                        <td>
                            {{ $bloomStartPeriod->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bloomStartPeriod.fields.bloom_start_period') }}
                        </th>
                        <td>
                            {{ $bloomStartPeriod->bloom_start_period }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bloomStartPeriod.fields.observation') }}
                        </th>
                        <td>
                            {{ $bloomStartPeriod->observation }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bloom-start-periods.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection