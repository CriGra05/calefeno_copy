@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.bloomStartPeriod.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.bloom-start-periods.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="bloom_start_period">{{ trans('cruds.bloomStartPeriod.fields.bloom_start_period') }}</label>
                <input class="form-control {{ $errors->has('bloom_start_period') ? 'is-invalid' : '' }}" type="text" name="bloom_start_period" id="bloom_start_period" value="{{ old('bloom_start_period', '') }}">
                @if($errors->has('bloom_start_period'))
                    <div class="invalid-feedback">
                        {{ $errors->first('bloom_start_period') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.bloomStartPeriod.fields.bloom_start_period_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="observation">{{ trans('cruds.bloomStartPeriod.fields.observation') }}</label>
                <input class="form-control {{ $errors->has('observation') ? 'is-invalid' : '' }}" type="text" name="observation" id="observation" value="{{ old('observation', '') }}">
                @if($errors->has('observation'))
                    <div class="invalid-feedback">
                        {{ $errors->first('observation') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.bloomStartPeriod.fields.observation_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection