@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.plantObserved.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.plant-observeds.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.id') }}
                        </th>
                        <td>
                            {{ $plantObserved->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.plant_observed_name') }}
                        </th>
                        <td>
                            {{ $plantObserved->plant_observed_name->plant_observed_name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.plant_name') }}
                        </th>
                        <td>
                            {{ $plantObserved->plant_name->plant_name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.notes') }}
                        </th>
                        <td>
                            {{ $plantObserved->notes }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.altitude') }}
                        </th>
                        <td>
                            {{ $plantObserved->altitude }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.coordinates') }}
                        </th>
                        <td>
                            {{ $plantObserved->coordinates }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.sun_exposure') }}
                        </th>
                        <td>
                            {{ $plantObserved->sun_exposure->sun_exposure ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.slope') }}
                        </th>
                        <td>
                            {{ $plantObserved->slope->terrain_slope ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.sorroundings') }}
                        </th>
                        <td>
                            {{ $plantObserved->sorroundings->sorroundings ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plantObserved.fields.observed_zone') }}
                        </th>
                        <td>
                            {{ $plantObserved->observed_zone->zone_name ?? '' }}
                        </td>
                    </tr>
                    <!-- Additional code to display user ID and username -->
                    <tr>
                        <th>
                            User ID
                        </th>
                        <td>
                            {{ $phenologicalObservation->user->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                                User Name
                        </th>
                        <td>
                            {{ $phenologicalObservation->user->name }}
                        </td>
                    </tr>
                    <!-- End of additional code -->
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.plant-observeds.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
