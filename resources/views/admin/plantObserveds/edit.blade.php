@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.plantObserved.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.plant-observeds.update", [$plantObserved->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <!--nuovo campo: nome pianta osservata-->
            <div class="form-group">
                <label for="plant_observed_name">{{ trans('cruds.plantObserved.fields.plant_observed_name') }}</label>
                <input class="form-control {{ $errors->has('plant_observed_name') ? 'is-invalid' : '' }}" type="text" name="plant_observed_name" id="plant_observed_name" value="{{ old('plant_observed_name', $plantObserved->plant_observed_name) }}">
                @if($errors->has('plant_observed_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('plant_observed_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.plant_observed_name_helper') }}</span>
            </div>
            <!--nuovo campo-->
            <div class="form-group">
                <label for="notes">{{ trans('cruds.plantObserved.fields.notes') }}</label>
                <input class="form-control {{ $errors->has('notes') ? 'is-invalid' : '' }}" type="text" name="notes" id="notes" value="{{ old('notes', $plantObserved->notes) }}">
                @if($errors->has('notes'))
                    <div class="invalid-feedback">
                        {{ $errors->first('notes') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.notes_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="altitude">{{ trans('cruds.plantObserved.fields.altitude') }}</label>
                <input class="form-control {{ $errors->has('altitude') ? 'is-invalid' : '' }}" type="text" name="altitude" id="altitude" value="{{ old('altitude', $plantObserved->altitude) }}">
                @if($errors->has('altitude'))
                    <div class="invalid-feedback">
                        {{ $errors->first('altitude') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.altitude_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="coordinates">{{ trans('cruds.plantObserved.fields.coordinates') }}</label>
                <input class="form-control {{ $errors->has('coordinates') ? 'is-invalid' : '' }}" type="text" name="coordinates" id="coordinates" value="{{ old('coordinates', $plantObserved->coordinates) }}">
                @if($errors->has('coordinates'))
                    <div class="invalid-feedback">
                        {{ $errors->first('coordinates') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.coordinates_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="sun_exposure_id">{{ trans('cruds.plantObserved.fields.sun_exposure') }}</label>
                <select class="form-control select2 {{ $errors->has('sun_exposure') ? 'is-invalid' : '' }}" name="sun_exposure_id" id="sun_exposure_id">
                    @foreach($sun_exposures as $id => $entry)
                        <option value="{{ $id }}" {{ (old('sun_exposure_id') ? old('sun_exposure_id') : $plantObserved->sun_exposure->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('sun_exposure'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sun_exposure') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.sun_exposure_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="slope_id">{{ trans('cruds.plantObserved.fields.slope') }}</label>
                <select class="form-control select2 {{ $errors->has('slope') ? 'is-invalid' : '' }}" name="slope_id" id="slope_id">
                    @foreach($slopes as $id => $entry)
                        <option value="{{ $id }}" {{ (old('slope_id') ? old('slope_id') : $plantObserved->slope->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('slope'))
                    <div class="invalid-feedback">
                        {{ $errors->first('slope') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.slope_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="sorroundings_id">{{ trans('cruds.plantObserved.fields.sorroundings') }}</label>
                <select class="form-control select2 {{ $errors->has('sorroundings') ? 'is-invalid' : '' }}" name="sorroundings_id" id="sorroundings_id">
                    @foreach($sorroundings as $id => $entry)
                        <option value="{{ $id }}" {{ (old('sorroundings_id') ? old('sorroundings_id') : $plantObserved->sorroundings->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('sorroundings'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sorroundings') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.sorroundings_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="plant_name_id">{{ trans('cruds.plantObserved.fields.plant_name') }}</label>
                <select class="form-control select2 {{ $errors->has('plant_name') ? 'is-invalid' : '' }}" name="plant_name_id" id="plant_name_id">
                    @foreach($plant_names as $id => $entry)
                        <option value="{{ $id }}" {{ (old('plant_name_id') ? old('plant_name_id') : $plantObserved->plant_name->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('plant_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('plant_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.plant_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="observed_zone_id">{{ trans('cruds.plantObserved.fields.observed_zone') }}</label>
                <select class="form-control select2 {{ $errors->has('observed_zone') ? 'is-invalid' : '' }}" name="observed_zone_id" id="observed_zone_id">
                    @foreach($observed_zones as $id => $entry)
                        <option value="{{ $id }}" {{ (old('observed_zone_id') ? old('observed_zone_id') : $plantObserved->observed_zone->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('observed_zone'))
                    <div class="invalid-feedback">
                        {{ $errors->first('observed_zone') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plantObserved.fields.observed_zone_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
