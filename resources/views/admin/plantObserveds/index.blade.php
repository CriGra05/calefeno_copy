@extends('layouts.admin')
@section('content')
@can('plant_observed_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.plant-observeds.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.plantObserved.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.plantObserved.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-PlantObserved">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.plant_observed_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.plant_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.notes') }}
                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.altitude') }}
                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.coordinates') }}
                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.sun_exposure') }}
                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.slope') }}
                        </th>
                        <th>
                            {{ trans('cruds.plantObserved.fields.sorroundings') }}
                        </th>

                        <th>
                            {{ trans('cruds.plantObserved.fields.observed_zone') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($plantObserveds as $key => $plantObserved)
                        <tr data-entry-id="{{ $plantObserved->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $plantObserved->id ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->plant_observed_name ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->plant_name->plant_name ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->notes ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->altitude ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->coordinates ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->sun_exposure->sun_exposure ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->slope->terrain_slope ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->sorroundings->sorroundings ?? '' }}
                            </td>
                            <td>
                                {{ $plantObserved->observed_zone->zone_name ?? '' }}
                            </td>
                            <td>
                                @can('plant_observed_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.plant-observeds.show', $plantObserved->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('plant_observed_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.plant-observeds.edit', $plantObserved->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('plant_observed_delete')
                                    <form action="{{ route('admin.plant-observeds.destroy', $plantObserved->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('plant_observed_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.plant-observeds.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-PlantObserved:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });

})

</script>
@endsection
