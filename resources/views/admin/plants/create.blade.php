@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.plant.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.plants.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="scientific_name">{{ trans('cruds.plant.fields.scientific_name') }}</label>
                <input class="form-control {{ $errors->has('scientific_name') ? 'is-invalid' : '' }}" type="text" name="scientific_name" id="scientific_name" value="{{ old('scientific_name', '') }}" required>
                @if($errors->has('scientific_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('scientific_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.scientific_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="species">{{ trans('cruds.plant.fields.species') }}</label>
                <input class="form-control {{ $errors->has('species') ? 'is-invalid' : '' }}" type="text" name="species" id="species" value="{{ old('species', '') }}">
                @if($errors->has('species'))
                    <div class="invalid-feedback">
                        {{ $errors->first('species') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.species_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.plant.fields.description') }}</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', '') }}">
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="minimum_age">{{ trans('cruds.plant.fields.minimum_age') }}</label>
                <input class="form-control {{ $errors->has('minimum_age') ? 'is-invalid' : '' }}" type="number" name="minimum_age" id="minimum_age" value="{{ old('minimum_age', '') }}" step="1">
                @if($errors->has('minimum_age'))
                    <div class="invalid-feedback">
                        {{ $errors->first('minimum_age') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.minimum_age_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="maximum_age">{{ trans('cruds.plant.fields.maximum_age') }}</label>
                <input class="form-control {{ $errors->has('maximum_age') ? 'is-invalid' : '' }}" type="number" name="maximum_age" id="maximum_age" value="{{ old('maximum_age', '') }}" step="1">
                @if($errors->has('maximum_age'))
                    <div class="invalid-feedback">
                        {{ $errors->first('maximum_age') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.maximum_age_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="plant_name">{{ trans('cruds.plant.fields.plant_name') }}</label>
                <input class="form-control {{ $errors->has('plant_name') ? 'is-invalid' : '' }}" type="text" name="plant_name" id="plant_name" value="{{ old('plant_name', '') }}">
                @if($errors->has('plant_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('plant_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.plant_name_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
