@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.blossomingTime.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.blossoming-times.update", [$blossomingTime->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name_blossoming_time">{{ trans('cruds.blossomingTime.fields.name_blossoming_time') }}</label>
                <input class="form-control {{ $errors->has('name_blossoming_time') ? 'is-invalid' : '' }}" type="text" name="name_blossoming_time" id="name_blossoming_time" value="{{ old('name_blossoming_time', $blossomingTime->name_blossoming_time) }}" required>
                @if($errors->has('name_blossoming_time'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_blossoming_time') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.blossomingTime.fields.name_blossoming_time_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="notes">{{ trans('cruds.blossomingTime.fields.notes') }}</label>
                <input class="form-control {{ $errors->has('notes') ? 'is-invalid' : '' }}" type="text" name="notes" id="notes" value="{{ old('notes', $blossomingTime->notes) }}">
                @if($errors->has('notes'))
                    <div class="invalid-feedback">
                        {{ $errors->first('notes') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.blossomingTime.fields.notes_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
