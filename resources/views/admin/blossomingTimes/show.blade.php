@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.blossomingTime.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.blossoming-times.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.blossomingTime.fields.id') }}
                        </th>
                        <td>
                            {{ $blossomingTime->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.blossomingTime.fields.name_blossoming_time') }}
                        </th>
                        <td>
                            {{ $blossomingTime->name_blossoming_time }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.blossomingTime.fields.notes') }}
                        </th>
                        <td>
                            {{ $blossomingTime->notes }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.blossoming-times.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
