@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.sorrounding.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.sorroundings.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.sorrounding.fields.id') }}
                        </th>
                        <td>
                            {{ $sorrounding->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.sorrounding.fields.sorroundings') }}
                        </th>
                        <td>
                            {{ $sorrounding->sorroundings }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.sorrounding.fields.description') }}
                        </th>
                        <td>
                            {{ $sorrounding->description }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.sorroundings.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection