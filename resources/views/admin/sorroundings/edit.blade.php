@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.sorrounding.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.sorroundings.update", [$sorrounding->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="sorroundings">{{ trans('cruds.sorrounding.fields.sorroundings') }}</label>
                <input class="form-control {{ $errors->has('sorroundings') ? 'is-invalid' : '' }}" type="text" name="sorroundings" id="sorroundings" value="{{ old('sorroundings', $sorrounding->sorroundings) }}">
                @if($errors->has('sorroundings'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sorroundings') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.sorrounding.fields.sorroundings_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.sorrounding.fields.description') }}</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', $sorrounding->description) }}">
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.sorrounding.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection