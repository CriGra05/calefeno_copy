@extends('layouts.admin')


@section('content')
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        CaleFeno
                    </div>

                    <div class="card-body">
                        <div class="mt-3">
                            <a href="{{ route('admin.phenological-observations.create') }}" class="btn btn-primary">
                                {{ trans('cruds.phenologicalObservation.fields.add_new') }}
                            </a>
                            <a href="{{ route('admin.plant-observeds.create') }}" class="btn btn-primary">
                                {{ trans('cruds.plantObserved.fields.add_new') }}
                            </a>
                        </div>
                        <div id="map" style="height: 400px"></div>

                        <div class="statistics-container">
                            <h4>Statistics:</h4>
                            <div class="statistic">
                                <div class="statistic-label">Piante osservate:</div>
                                <div class="statistic-value">{{ isset($totalPlantsObserved) ? $totalPlantsObserved : 'not found' }}</div>
                            </div>
                            <div class="statistic">
                                <div class="statistic-label">Osservazioni fenologiche:</div>
                                <div class="statistic-value">{{ isset($totalPhenologicalObservations) ? $totalPhenologicalObservations : 'not found' }}</div>
                            </div>

                            {{-- <?php         var_dump($totalPlantsObserved); ?> --}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/map.js') }}"></script>
@endsection



