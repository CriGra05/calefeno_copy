<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            {{ trans('panel.site_title') }}
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @can('user_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/permissions*") ? "c-show" : "" }} {{ request()->is("admin/roles*") ? "c-show" : "" }} {{ request()->is("admin/users*") ? "c-show" : "" }} {{ request()->is("admin/people*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('permission_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-unlock-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-briefcase c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('person_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.people.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/people") || request()->is("admin/people/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user-circle c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.person.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('all_plant_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/plants*") ? "c-show" : "" }} {{ request()->is("admin/bloom_end_period_access*") ? "c-show" : "" }} {{ request()->is("admin/plant-images*") ? "c-show" : "" }} {{ request()->is("admin/flower-images*") ? "c-show" : "" }} {{ request()->is("admin/bloom-start-periods*") ? "c-show" : "" }} {{ request()->is("admin/bloom-end-periods*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-tree c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.allPlant.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('plant_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.plants.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/plants") || request()->is("admin/plants/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-tree c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.plant.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('plant_image_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.plant-images.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/plant-images") || request()->is("admin/plant-images/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-tree c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.plantImage.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('flower_image_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.flower-images.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/flower-images") || request()->is("admin/flower-images/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-spa c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.flowerImage.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('bloom_start_period_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.bloom-start-periods.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/bloom-start-periods") || request()->is("admin/bloom-start-periods/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-spa c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.bloomStartPeriod.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('bloom_end_period_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.bloom-end-periods.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/bloom-end-periods") || request()->is("admin/bloom-end-periods/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-spa c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.bloomEndPeriod.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('observed_plant_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/plant-observeds*") ? "c-show" : "" }} {{ request()->is("admin/observation-zones*") ? "c-show" : "" }} {{ request()->is("admin/sun-exposures*") ? "c-show" : "" }} {{ request()->is("admin/slopes*") ? "c-show" : "" }} {{ request()->is("admin/sorroundings*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-binoculars c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.observedPlant.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('plant_observed_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.plant-observeds.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/plant-observeds") || request()->is("admin/plant-observeds/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-tree c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.plantObserved.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('observation_zone_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.observation-zones.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/observation-zones") || request()->is("admin/observation-zones/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-map-marked c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.observationZone.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('location_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.locations.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/locations") || request()->is("admin/locations/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-map-marked-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.location.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('sun_exposure_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.sun-exposures.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/sun-exposures") || request()->is("admin/sun-exposures/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-sun c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.sunExposure.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('slope_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.slopes.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/slopes") || request()->is("admin/slopes/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-percent c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.slope.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('sorrounding_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.sorroundings.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/sorroundings") || request()->is("admin/sorroundings/*") ? "c-active" : "" }}">
                                <i class="fa-fw fab fa-envira c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.sorrounding.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('phenologial_observation_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/blossoming-times*") ? "c-show" : "" }} {{ request()->is("admin/phenological-observations*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-search c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.phenologialObservation.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">

                    @can('phenological_observation_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.phenological-observations.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/phenological-observations") || request()->is("admin/phenological-observations/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-align-justify c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.phenologicalObservation.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('blossoming_time_access')
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.blossoming-times.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/blossoming-times") || request()->is("admin/blossoming-times/*") ? "c-active" : "" }}">
                            <i class="fa-fw fas fa-spa c-sidebar-nav-icon">

                            </i>
                            {{ trans('cruds.blossomingTime.title') }}
                        </a>
                    </li>
                @endcan
                </ul>
            </li>
        @endcan
        @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
            @can('profile_password_edit')
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'c-active' : '' }}" href="{{ route('profile.password.edit') }}">
                        <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                        </i>
                        {{ trans('global.change_password') }}
                    </a>
                </li>
            @endcan
        @endif

        {{-- add a link to redirect to the homepage --}}
        <li class="c-sidebar-nav-item">
            <a href="{{ route('welcome') }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-home"></i>
                {{-- {{ trans('global.home') }} --}}
                Ritorna al sito
            </a>
        </li>

        {{-- Logout button --}}
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">
                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>
