@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card mx-4">
                <div class="card-body p-4">
                    <h1>Contattaci</h1>

    <form action="{{ route('contact.submit') }}" method="POST">
        @csrf

        <div class="form-group mb-3">
            <label for="name">Name:</label>
            <div>
            <input type="text" id="name" name="name" value="{{ old('name') }}" required>
            @error('name')
                <span class="text-red-500">{{ $message }}</span>
            @enderror
            </div>
        </div>

        <div class="form-group mb-3">
            <label for="email">Email:</label>
            <div>
            <input type="email" id="email" name="email" value="{{ old('email') }}" required>
            @error('email')
                <span class="text-red-500">{{ $message }}</span>
            @enderror
            </div>
        </div>

        <div class="form-group mb-3">
            <label for="message">Message:</label>
            <div>
                <textarea class="form-group " id="message" name="message" required>{{ old('message') }}</textarea>
                @error('message')
                <span class="text-red-1000">{{ $message }}</span>
                @enderror
            </div>

        </div>

        <div class="form-group">
            <button type="submit">Submit</button>
        </div>
    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
