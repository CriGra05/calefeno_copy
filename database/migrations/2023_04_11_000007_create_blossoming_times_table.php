<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlossomingTimesTable extends Migration
{
    public function up()
    {
        Schema::create('blossoming_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_blossoming_time')->unique();
            $table->string('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
