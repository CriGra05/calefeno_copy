<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhenologicalObservationsTable extends Migration
{
    public function up()
    {
        Schema::create('phenological_observations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('notes')->nullable();
            $table->string('weather_conditions')->nullable();

            // Add foreign key column
            //$table->unsignedBigInteger('plant_observed_id');
            //$table->foreign('plant_observed_id')->references('id')->on('plant_observeds');

            $table->timestamps();
            $table->softDeletes();
        });
    }
}
