<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * add the foreign keys in the phenological_observations: blossoming_times_id user_id
     */
    public function up()
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->unsignedBigInteger('blossoming_times_id')->nullable()->change();
            $table->foreign('blossoming_times_id', 'phenological_observations_blossoming_times_id_foreign')->references('id')->on('blossoming_times');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id', 'phenological_observations_user_id_foreign')->references('id')->on('users');
        });
    }


    /**
     * Reverse the migrations.
     * Delete the current table and add new table
     */
    public function down()
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->dropForeign(['blossoming_times_id']);
            $table->dropForeign(['user_id']);
            $table->dropColumn(['blossoming_times_id', 'user_id']);
        });
    }

};

