<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlopesTable extends Migration
{
    public function up()
    {
        Schema::create('slopes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('terrain_slope')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
