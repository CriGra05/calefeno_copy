<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('blossoming_times', function (Blueprint $table) {
            $table->renameColumn('name_blossowing_time','name_blossoming_time');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('blossoming_times', function (Blueprint $table) {
            $table->renameColumn('name_blossoming_time', 'name_blossowing_time');
        });
    }
};
