<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloomEndPeriodsTable extends Migration
{
    public function up()
    {
        Schema::create('bloom_end_periods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bloom_end_period')->nullable();
            $table->string('observation')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
