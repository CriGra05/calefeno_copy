<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * Add nullable property to blossoming_times_id foregn key into phenological_observations
     */
    public function up()
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->unsignedBigInteger('blossoming_times_id')->nullable();

            $table->foreign('blossoming_times_id')
                ->references('id')
                ->on('blossoming_times')
                ->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     * Rollback
     */
    public function down(): void
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            //
        });
    }
};
