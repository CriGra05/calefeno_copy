<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPlantObservedsTable extends Migration
{
    public function up()
    {
        Schema::table('plant_observeds', function (Blueprint $table) {
            $table->unsignedBigInteger('sun_exposure_id')->nullable();
            $table->foreign('sun_exposure_id', 'sun_exposure_fk_8235558')->references('id')->on('sun_exposures');
            $table->unsignedBigInteger('slope_id')->nullable();
            $table->foreign('slope_id', 'slope_fk_8235586')->references('id')->on('slopes');
            $table->unsignedBigInteger('sorroundings_id')->nullable();
            $table->foreign('sorroundings_id', 'sorroundings_fk_8235598')->references('id')->on('sorroundings');
            $table->unsignedBigInteger('plant_name_id')->nullable();
            $table->foreign('plant_name_id', 'plant_name_fk_8235610')->references('id')->on('plants');
            $table->unsignedBigInteger('observed_zone_id')->nullable();
            $table->foreign('observed_zone_id', 'observed_zone_fk_8312273')->references('id')->on('observation_zones');
        });
    }
}
