<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPhenologicalObservationsTable extends Migration
{
    public function up()
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->unsignedBigInteger('blossoming_time_id')->nullable();
            $table->foreign('blossoming_time_id', 'blossoming_time_fk')->references('id')->on('blossoming_times');


            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id','user_id_fk')->references('id')->on('user_id');
        });
    }

    public function down(){
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->dropColumn('blossoming_time_id');
            $table->dropForeign(['blossoming_time_fk_8235522']);
        });

    }
}
