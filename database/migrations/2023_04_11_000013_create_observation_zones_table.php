<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationZonesTable extends Migration
{
    public function up()
    {
        Schema::create('observation_zones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('zone_name')->nullable();
            $table->integer('zip_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
