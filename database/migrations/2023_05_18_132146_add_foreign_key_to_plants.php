<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->unsignedBigInteger('bloom_start_period_id')->nullable();
            $table->unsignedBigInteger('bloom_end_period_id')->nullable();

            $table->foreign('bloom_start_period_id')
                ->references('id')
                ->on('bloom_start_periods');

            $table->foreign('bloom_end_period_id')
                ->references('id')
                ->on('bloom_end_periods');
        });
    }

    public function down()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->dropForeign(['bloom_start_period_id']);
            $table->dropForeign(['bloom_end_period_id']);
            $table->dropColumn(['bloom_start_period_id', 'bloom_end_period_id']);
        });
    }
};


