<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSorroundingsTable extends Migration
{
    public function up()
    {
        Schema::create('sorroundings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sorroundings')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
