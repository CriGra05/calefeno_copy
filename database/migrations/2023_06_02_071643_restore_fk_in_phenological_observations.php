<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * I will drop a error column name in the phenological_observations table
     * because the id isn't blossowing_time_id but blossoming_times_id
     */
    public function up(): void
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->dropForeign(['blossowing_time_fk_8235522']);
            $table->dropColumn('blossowing_time_id');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->unsignedBigInteger('blossoming_times_id')->nullable();
            $table->foreign('blossomings_time_id', 'blossoming_times_fk')->references('id')->on('blossoming_times');


            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id','user_id_fk')->references('id')->on('user_id');
        });
    }
};
