<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloomStartPeriodsTable extends Migration
{
    public function up()
    {
        Schema::create('bloom_start_periods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bloom_start_period')->nullable();
            $table->string('observation')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
