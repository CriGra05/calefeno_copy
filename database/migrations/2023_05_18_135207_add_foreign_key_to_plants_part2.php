<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->unsignedBigInteger('plant_images_id');
            $table->unsignedBigInteger('flower_images_id');

            $table->foreign('plant_images_id')
            ->references('id')
            ->on('plant_images');

            $table->foreign('flower_images_id')
                  ->references('id')
                  ->on('flower_images');


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->dropForeign(['plant_images_id']);
            $table->dropForeign(['flower_images_id']);
            $table->dropColumn(['plant_images_id', 'flower_images_id']);
        });
    }
};
