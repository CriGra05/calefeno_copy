<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToLocationsTable extends Migration
{
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->unsignedBigInteger('zip_code_id')->nullable();
            $table->foreign('zip_code_id', 'zip_code_fk_8236695')->references('id')->on('locations');
        });
    }
}
