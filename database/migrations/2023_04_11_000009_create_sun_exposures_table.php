<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSunExposuresTable extends Migration
{
    public function up()
    {
        Schema::create('sun_exposures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sun_exposure')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
