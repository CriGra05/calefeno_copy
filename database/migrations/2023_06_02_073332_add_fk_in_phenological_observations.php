<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            //$table->unsignedBigInteger('blossoming_times_id')->nullable();
            //$table->foreign('blossoming_time_id', 'blossoming_time_fk')->references('id')->on('blossoming_times');


            //$table->unsignedBigInteger('user_id')->nullable();
            //$table->foreign('user_id','user_id_fk')->references('id')->on('users');
            $table->foreign('plant_observed_id','plant_observed_fk')->references('id')->on('plant_observeds');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->dropForeign(['blossoming_times_fk']);
            $table->dropColumn('blossoming_times_id');

            $table->dropForeign('user_id_fk');
            $table->dropColumn('user_id');

            //
        });
    }
};
