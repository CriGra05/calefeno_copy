<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->unsignedBigInteger('plant_observed_id')->nullable();

            $table->foreign('plant_observed_id')
                  ->references('id')
                  ->on('plant_observeds');
        });
    }


    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('phenological_observations', function (Blueprint $table) {
            $table->dropForeign(['plant_observed_id']);
        });
    }

};
