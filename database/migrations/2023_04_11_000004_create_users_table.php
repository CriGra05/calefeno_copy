<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //original users table fields
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->datetime('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();

            // fields added from the 'people' table
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('address')->nullable();
            $table->integer('postcode')->nullable();
            $table->string('place')->nullable();
            $table->string('company')->nullable();
            $table->integer('phone')->nullable();
            $table->string('mobile')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }
}
