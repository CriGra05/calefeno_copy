<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'person_create',
            ],
            [
                'id'    => 18,
                'title' => 'person_edit',
            ],
            [
                'id'    => 19,
                'title' => 'person_show',
            ],
            [
                'id'    => 20,
                'title' => 'person_delete',
            ],
            [
                'id'    => 21,
                'title' => 'person_access',
            ],
            [
                'id'    => 22,
                'title' => 'phenological_observation_create',
            ],
            [
                'id'    => 23,
                'title' => 'phenological_observation_edit',
            ],
            [
                'id'    => 24,
                'title' => 'phenological_observation_show',
            ],
            [
                'id'    => 25,
                'title' => 'phenological_observation_delete',
            ],
            [
                'id'    => 26,
                'title' => 'phenological_observation_access',
            ],
            [
                'id'    => 27,
                'title' => 'blossoming_time_create',
            ],
            [
                'id'    => 28,
                'title' => 'blossoming_time_edit',
            ],
            [
                'id'    => 29,
                'title' => 'blossoming_time_show',
            ],
            [
                'id'    => 30,
                'title' => 'blossoming_time_delete',
            ],
            [
                'id'    => 31,
                'title' => 'blossoming_time_access',
            ],
            [
                'id'    => 32,
                'title' => 'plant_observed_create',
            ],
            [
                'id'    => 33,
                'title' => 'plant_observed_edit',
            ],
            [
                'id'    => 34,
                'title' => 'plant_observed_show',
            ],
            [
                'id'    => 35,
                'title' => 'plant_observed_delete',
            ],
            [
                'id'    => 36,
                'title' => 'plant_observed_access',
            ],
            [
                'id'    => 37,
                'title' => 'sun_exposure_create',
            ],
            [
                'id'    => 38,
                'title' => 'sun_exposure_edit',
            ],
            [
                'id'    => 39,
                'title' => 'sun_exposure_show',
            ],
            [
                'id'    => 40,
                'title' => 'sun_exposure_delete',
            ],
            [
                'id'    => 41,
                'title' => 'sun_exposure_access',
            ],
            [
                'id'    => 42,
                'title' => 'slope_create',
            ],
            [
                'id'    => 43,
                'title' => 'slope_edit',
            ],
            [
                'id'    => 44,
                'title' => 'slope_show',
            ],
            [
                'id'    => 45,
                'title' => 'slope_delete',
            ],
            [
                'id'    => 46,
                'title' => 'slope_access',
            ],
            [
                'id'    => 47,
                'title' => 'sorrounding_create',
            ],
            [
                'id'    => 48,
                'title' => 'sorrounding_edit',
            ],
            [
                'id'    => 49,
                'title' => 'sorrounding_show',
            ],
            [
                'id'    => 50,
                'title' => 'sorrounding_delete',
            ],
            [
                'id'    => 51,
                'title' => 'sorrounding_access',
            ],
            [
                'id'    => 52,
                'title' => 'plant_create',
            ],
            [
                'id'    => 53,
                'title' => 'plant_edit',
            ],
            [
                'id'    => 54,
                'title' => 'plant_show',
            ],
            [
                'id'    => 55,
                'title' => 'plant_delete',
            ],
            [
                'id'    => 56,
                'title' => 'plant_access',
            ],
            [
                'id'    => 57,
                'title' => 'observation_zone_create',
            ],
            [
                'id'    => 58,
                'title' => 'observation_zone_edit',
            ],
            [
                'id'    => 59,
                'title' => 'observation_zone_show',
            ],
            [
                'id'    => 60,
                'title' => 'observation_zone_delete',
            ],
            [
                'id'    => 61,
                'title' => 'observation_zone_access',
            ],
            [
                'id'    => 62,
                'title' => 'location_create',
            ],
            [
                'id'    => 63,
                'title' => 'location_edit',
            ],
            [
                'id'    => 64,
                'title' => 'location_show',
            ],
            [
                'id'    => 65,
                'title' => 'location_delete',
            ],
            [
                'id'    => 66,
                'title' => 'location_access',
            ],
            [
                'id'    => 67,
                'title' => 'plant_image_create',
            ],
            [
                'id'    => 68,
                'title' => 'plant_image_edit',
            ],
            [
                'id'    => 69,
                'title' => 'plant_image_show',
            ],
            [
                'id'    => 70,
                'title' => 'plant_image_delete',
            ],
            [
                'id'    => 71,
                'title' => 'plant_image_access',
            ],
            [
                'id'    => 72,
                'title' => 'flower_image_create',
            ],
            [
                'id'    => 73,
                'title' => 'flower_image_edit',
            ],
            [
                'id'    => 74,
                'title' => 'flower_image_show',
            ],
            [
                'id'    => 75,
                'title' => 'flower_image_delete',
            ],
            [
                'id'    => 76,
                'title' => 'flower_image_access',
            ],
            [
                'id'    => 77,
                'title' => 'bloom_start_period_create',
            ],
            [
                'id'    => 78,
                'title' => 'bloom_start_period_edit',
            ],
            [
                'id'    => 79,
                'title' => 'bloom_start_period_show',
            ],
            [
                'id'    => 80,
                'title' => 'bloom_start_period_delete',
            ],
            [
                'id'    => 81,
                'title' => 'bloom_start_period_access',
            ],
            [
                'id'    => 82,
                'title' => 'bloom_end_period_create',
            ],
            [
                'id'    => 83,
                'title' => 'bloom_end_period_edit',
            ],
            [
                'id'    => 84,
                'title' => 'bloom_end_period_show',
            ],
            [
                'id'    => 85,
                'title' => 'bloom_end_period_delete',
            ],
            [
                'id'    => 86,
                'title' => 'bloom_end_period_access',
            ],
            [
                'id'    => 87,
                'title' => 'all_plant_access',
            ],
            [
                'id'    => 88,
                'title' => 'observed_plant_access',
            ],
            [
                'id'    => 89,
                'title' => 'phenologial_observation_access',
            ],
            [
                'id'    => 90,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
