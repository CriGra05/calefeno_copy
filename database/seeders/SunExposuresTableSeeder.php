<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SunExposuresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sun_exposures')->delete();
        
        \DB::table('sun_exposures')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sun_exposure' => 'Nord',
                'notes' => NULL,
                'created_at' => '2023-04-29 06:35:49',
                'updated_at' => '2023-04-29 06:35:49',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'sun_exposure' => 'Sud',
                'notes' => NULL,
                'created_at' => '2023-04-29 06:35:58',
                'updated_at' => '2023-04-29 06:35:58',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}