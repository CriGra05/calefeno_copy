// Wait for the DOM to be ready
document.addEventListener('DOMContentLoaded', function() {
    // Map initialization code
    var map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
      maxZoom: 18
    }).addTo(map);

    // Add markers, polygons, and other features to the map as needed

    var marker = L.marker([51.5, -0.09]).addTo(map);

    // Get the user's current position
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        function(position) {
          var lat = position.coords.latitude;
          var lon = position.coords.longitude;

          // Create a marker for the user's position
          var userMarker = L.marker([lat, lon]).addTo(map);

          // Update the map view to center on the user's position
          map.setView([lat, lon], 13);
        },
        function(error) {
          console.log('Error getting user location:', error);
        }
      );
    } else {
      console.log('Geolocation is not supported by this browser.');
    }
  });

